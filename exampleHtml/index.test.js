const assert = require('assert')
const {getInnerHTML} = require('./index')
const jsdom = require('jsdom')
const {JSDOM} = jsdom

let document

describe('Html Example', () => {
  beforeEach(() => {
    return JSDOM.fromFile('exampleHtml/index.html').then(dom => {
      document = dom.window.document
    })
  })
  it('should log element from html', () => {
    const paragraph = document.getElementById('paragraph')
    assert.equal(getInnerHTML(paragraph), 'My first paragraph.')
  })
})
