function getInnerHTML(element) {
  return element.innerHTML
}

module.exports = {
  getInnerHTML
}
