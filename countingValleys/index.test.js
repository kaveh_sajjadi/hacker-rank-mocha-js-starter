const assert = require('assert')
const {countingValleys} = require('./index')

describe('Counting Valleys', () => {
  it('should get valley count', () => {
    const steps = 'DDUUUUDD'
    const valleyCount = 1

    assert.equal(countingValleys(steps.length, steps), valleyCount)
  })
})
