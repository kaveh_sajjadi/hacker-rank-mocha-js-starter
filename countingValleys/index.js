function countingValleys(n, s) {
  let level = 0
  let valleyCount = 0
  const stepsArray = s.split('')
  stepsArray.forEach(step => {
    let prevLevel = level
    if (step === 'D') level -= 1
    if (step === 'U') level += 1
    if (prevLevel >= 0 && level < 0) valleyCount += 1
  })

  return valleyCount
}

module.exports = {
  countingValleys
}
