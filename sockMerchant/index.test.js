const assert = require('assert')
const {sockMerchant} = require('./index')

describe('Sock Merchant', () => {
  it('should get pairs from socks', () => {
    const ar = [1, 2, 1, 2, 1, 3, 2]
    const pairs = 2

    assert.equal(sockMerchant(ar.length, ar), pairs)
  })
})
