function sockMerchant(n, sockColors) {
  var reducer = (totSockColors, currentSockColor) => {
    if (!totSockColors[currentSockColor]) {
      totSockColors[currentSockColor] = 1
    } else {
      totSockColors[currentSockColor] = totSockColors[currentSockColor] + 1
    }
    return totSockColors
  }
  var sockColorsCounts = sockColors.reduce(reducer, {})

  let count = 0

  Object.values(sockColorsCounts).forEach(color => {
    if (Math.floor(color / 2) > 0) count = count + Math.floor(color / 2)
  })

  return count
}

module.exports = {
  sockMerchant
}
