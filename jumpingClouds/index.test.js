const assert = require('assert')
const {jumpingClouds} = require('./index')

describe('Jumping Clouds', () => {
  it('should get number of jumps', () => {
    const c = [0, 0, 1, 0, 0, 1, 0]
    const jumps = 4

    assert.equal(jumpingClouds(c), jumps)
  })
})
