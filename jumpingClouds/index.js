function jumpingClouds(c) {
  let jumps = 0

  for (let i = 0; i < c.length; i++) {
    if (c[i + 1] === 0 && c[i + 2] === 0) {
      i += 1
      jumps += 1
    } else if (c[i + 1] === 0 && c[i + 2] === 1) {
      jumps += 1
    }
    if (c[i + 1] === 1) {
      jumps += 1
    }
  }
  return jumps
}

module.exports = {
  jumpingClouds
}
